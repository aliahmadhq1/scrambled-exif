New in 1.7.8
★ Update Dutch and French translations.
★ Upgrade a bunch of dependencies.

New in 1.7.7
★ Update traditional Chinese, Czech, Finnish and French translations.

New in 1.7.6
★ Upgrade a lot of dependencies.

New in 1.7.5
★ Update Portuguese (Portugal) and Belarusian translations.
